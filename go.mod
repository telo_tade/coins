module gitlab.com/telo_tade/coins

go 1.15

require (
	github.com/go-log/log v0.2.0
	github.com/google/subcommands v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.6.0
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.5.1
	golang.org/x/sys v0.0.0-20200223170610-d5e6a3e2c0ae // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.3.0
)

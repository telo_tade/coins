package combine_test

import (
	"reflect"
	"testing"

	"gitlab.com/telo_tade/coins/pkg/combine"
	"gitlab.com/telo_tade/coins/pkg/models"
)

func TestCombine(t *testing.T) {
	type args struct {
		ranks  []string
		prices map[string]float64
	}

	tests := []struct {
		name    string
		args    args
		want    []models.Coin
		wantErr bool
	}{
		{
			name: "normal run",
			args: args{
				ranks: []string{"BTC", "ETH", "LINK", "YFI"},
				prices: map[string]float64{
					"YFI":  18.18,
					"ETH":  10.10,
					"LINK": 19.19,
					"BTC":  11.11,
				},
			},
			wantErr: false,
			want: []models.Coin{
				{Name: "BTC", Price: 11.11, Rank: 0},
				{Name: "ETH", Price: 10.10, Rank: 1},
				{Name: "LINK", Price: 19.19, Rank: 2},
				{Name: "YFI", Price: 18.18, Rank: 3},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := combine.Combine(tt.args.ranks, tt.args.prices)

			if (err != nil) != tt.wantErr {
				t.Errorf("Combine() error = %v, wantErr %v", err, tt.wantErr)

				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Combine() = %v, want %v", got, tt.want)
			}
		})
	}
}

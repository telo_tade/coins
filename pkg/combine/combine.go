/*
Package combine offers functionality to:
- combine a list of rankings and a list of prices to produce a list of top rated coins (slice of Coin)
- use a rankins message queue, and a prices message queue to produce the list of top rated coins

Note: once creating a client, it is your responsibility to Close() its connections & channels.
I have not found, so far, a way to elegantly motivate the user to do this.
*/
package combine

import (
	"encoding/json"
	"time"

	"github.com/go-log/log"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"gitlab.com/telo_tade/coins/pkg/models"
)

const pricesQueueName = "prices_queue"

const rankingsQueueName = "rankings_queue"

// Combine takes a list of ranks and one of prices and composes a correspopnding slice of coins.
func Combine(ranks []string, prices map[string]float64) ([]models.Coin, error) {
	if len(ranks) == 0 {
		return nil, errors.New("Ranks cannot be nil or empty")
	}

	if len(prices) == 0 {
		return nil, errors.New("Prices cannot be nil or empty")
	}

	result := make([]models.Coin, len(ranks))

	for i := 0; i < len(ranks); i++ {
		result[i] = models.Coin{
			Name:  ranks[i],
			Rank:  i,
			Price: prices[ranks[i]],
		}
	}

	return result, nil
}

// Client encapsulates functionality to compute the top list based on prices and rankings queues.
type Client struct {
	L                          log.Logger
	PricesQueueURL             string
	RankingsQueueURL           string
	pricesConn, rankingsConn   *amqp.Connection
	pricesChann, rankingsChann *amqp.Channel
}

// New returns a new client.
func New(pricesQueueURL, rankingsQueueURL string, l log.Logger) (*Client, error) {
	result := Client{
		L:                l,
		PricesQueueURL:   pricesQueueURL,
		RankingsQueueURL: rankingsQueueURL,
	}

	pricesConn, rankingsConn := result.createConnections()
	if pricesConn == nil || rankingsConn == nil {
		return nil, errors.Errorf("Could not create connections")
	}

	result.pricesConn, result.rankingsConn = pricesConn, rankingsConn

	pricesChann, rankingsChann := result.createChannels()
	if pricesChann == nil || rankingsChann == nil {
		result.closeConnections()

		return nil, errors.Errorf("Could not create channels")
	}

	result.pricesChann, result.rankingsChann = pricesChann, rankingsChann

	return &result, nil
}

func (c *Client) createConnections() (*amqp.Connection, *amqp.Connection) {
	pricesConn, err := amqp.Dial(c.PricesQueueURL)
	if err != nil {
		c.L.Logf("Error creating connection for %v: %v", c.PricesQueueURL, err.Error())
	}

	rankingsConn, err := amqp.Dial(c.RankingsQueueURL)
	if err != nil {
		c.L.Logf("Error creating connection for %v: %v", c.RankingsQueueURL, err.Error())
	}

	return pricesConn, rankingsConn
}

func (c *Client) closeConnections() {
	if c.pricesConn != nil {
		c.pricesConn.Close()
	}

	if c.rankingsConn != nil {
		c.rankingsConn.Close()
	}
}

func (c *Client) createChannels() (*amqp.Channel, *amqp.Channel) {
	prices, err := c.pricesConn.Channel()
	if err != nil {
		c.L.Logf("Error creating channel for prices connection")
	}

	rankings, err := c.rankingsConn.Channel()
	if err != nil {
		c.L.Logf("Error creating channel for rankings connection")
	}

	return prices, rankings
}

// Close tries to close unused resources.
func (c *Client) Close() {
	c.closeChannels()
	c.closeConnections()
}

func (c *Client) closeChannels() {
	if c.pricesChann != nil {
		c.pricesChann.Close()
	}

	if c.rankingsChann != nil {
		c.rankingsChann.Close()
	}
}

func (c *Client) setQos() {
	err := c.pricesChann.Qos(1, 0, false)
	if err != nil {
		c.L.Logf("Error configuring prices channel: %v", err.Error())
	}

	err = c.rankingsChann.Qos(1, 0, false)
	if err != nil {
		c.L.Logf("Error configuring rankings channel: %v", err.Error())
	}
}

// GetAssets compiles and returns the top 100 assets.
func (c *Client) GetAssets() ([]models.Coin, error) {
	c.setQos()

	now := time.Now()

	rankings := c.readLatestRankings(now, 1*time.Second)
	prices := c.readLatestPrices(now, 1*time.Second)

	if len(rankings) == 0 || len(prices) == 0 {
		return []models.Coin{}, errors.Errorf("Not enough rankings or prices data to compose a response")
	}

	closestRank, closestPrice := closest(rankings, prices)

	return Combine(closestRank, closestPrice)
}

func closest(rankings map[time.Time][]string, prices map[time.Time]map[string]float64) ([]string, map[string]float64) {
	best := 1 * time.Second
	resultRanking, resultPrice := []string{}, map[string]float64{}

	for rankingTimestamp, ranking := range rankings {
		for priceTimestamp, price := range prices {
			interval := rankingTimestamp.Sub(priceTimestamp)
			if rankingTimestamp.Before(priceTimestamp) {
				interval = priceTimestamp.Sub(rankingTimestamp)
			}

			if interval < best {
				best, resultRanking, resultPrice = interval, ranking, price
			}
		}
	}

	return resultRanking, resultPrice
}

func (c *Client) readLatestPrices(before time.Time, interval time.Duration) map[time.Time]map[string]float64 {
	result := make(map[time.Time]map[string]float64)
	current := models.PriceMessage{}

	price, ok, err := c.pricesChann.Get(pricesQueueName, false)
	if err != nil {
		c.L.Logf("Error getting price: %v.", err.Error())
	}

	for ok && err == nil {
		err = price.Ack(false)
		if err != nil {
			c.L.Logf("Error acknowledging price message: %v.", err.Error())
		}

		unmarshalErr := json.Unmarshal(price.Body, &current)
		if unmarshalErr != nil {
			c.L.Logf("Error unmarshalling: %v.", unmarshalErr.Error())

			continue
		}

		// discard old prices & put back current ones
		if current.Timestamp.After(before.Add(-1 * interval)) {
			result[current.Timestamp] = current.Payload

			err = c.rankingsChann.Publish("", pricesQueueName, false, false, amqp.Publishing{
				DeliveryMode: amqp.Persistent,
				ContentType:  "text/plain",
				Body:         price.Body,
			})

			if err != nil {
				c.L.Logf("Error publishing back valid price %+v: %v.", current.Timestamp, err.Error())
			} else {
				c.L.Logf("Price %+v was placed back into the queue.", current.Timestamp)
			}

			// if we are seeing future data (means we have iterated on all sufficiently recent data)
			if current.Timestamp.After(before.Add(1 * interval)) {
				break
			}
		}

		price, ok, err = c.pricesChann.Get(pricesQueueName, false)
	}

	return result
}

func (c *Client) readLatestRankings(before time.Time, interval time.Duration) map[time.Time][]string {
	result := make(map[time.Time][]string)
	current := models.RankingsMessage{}

	ranking, ok, err := c.rankingsChann.Get(rankingsQueueName, false)
	if err != nil {
		c.L.Logf("Error getting ranking: %v.", err.Error())
	}

	for ok && err == nil {
		err = ranking.Ack(false)
		if err != nil {
			c.L.Logf("Error acknowledging ranking message: %v.", err.Error())
		}

		unmarshalErr := json.Unmarshal(ranking.Body, &current)
		if unmarshalErr != nil {
			c.L.Logf("Error unmarshalling: %v.", unmarshalErr.Error())

			err = ranking.Ack(false)
			if err != nil {
				c.L.Logf("Error acknowledging unmarshalled ranking message: %v.", err.Error())
			}

			continue
		}

		// discard old rankings & put back current ones
		if current.Timestamp.After(before.Add(-1 * interval)) {
			result[current.Timestamp] = current.Payload

			err = c.rankingsChann.Publish("", rankingsQueueName, false, false, amqp.Publishing{
				DeliveryMode: amqp.Persistent,
				ContentType:  "text/plain",
				Body:         ranking.Body,
			})

			if err != nil {
				c.L.Logf("Error publishing back valid ranking %+v: %v.", current.Timestamp, err.Error())
			} else {
				c.L.Logf("Ranking %+v was placed back into the queue.", current.Timestamp)
			}

			// if we are seeing future data (means we have iterated on all sufficiently recent data)
			if current.Timestamp.After(before.Add(1 * interval)) {
				break
			}
		}

		ranking, ok, err = c.rankingsChann.Get(rankingsQueueName, false)
	}

	return result
}

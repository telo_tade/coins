package models

// CoinInfo stores infor about one crypto currency.
type CoinInfo struct {
	ID        string `json:"Id"`
	Name      string `json:"Name"`
	FullName  string `json:"FullName"`
	Algorithm string `json:"Algorithm"`
}

// Data models info about one crypto currency.
type Data struct {
	CoinInfo CoinInfo `json:"CoinInfo"`
}

// RankResponse models a response from min-api.cryptocompare.com.
type RankResponse struct {
	Response      string
	Message       string
	Type          int
	SponsoredData []Data `json:"SponsoredData"`
	Data          []Data `json:"Data"`
	HasWarning    bool
}

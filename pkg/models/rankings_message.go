package models

import "time"

// RankingsMessage encapsulates a rankings reading and a timestamp.
type RankingsMessage struct {
	Payload   []string
	Timestamp time.Time
}

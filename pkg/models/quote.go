package models

import "time"

// Status models the returned status of a coinmarketcap request.
type Status struct {
	Timestamp    time.Time `json:"timestamp"`
	ErrorCode    int       `json:"error_code"`
	ErrorMessage string    `json:"error_message"`
	Elapsed      int       `json:"elapsed"`
	CreditCount  int       `json:"credit_count"`
}

// Currency models the coinmarketcap price for a crypto currency in a given real currency.
type Currency struct {
	Price            float64   `json:"price"`
	Volume24h        float64   `json:"volume_24h"`
	PercentChange1h  float64   `json:"percent_change_1h"`
	PercentChange24h float64   `json:"percent_change_24h"`
	PercentChange7d  float64   `json:"percent_change_7d"`
	MarketCap        float64   `json:"market_cap"`
	LastUpdated      time.Time `json:"last_updated"`
}

// Platform models the coinmarketcap price for a crypto currency in a given real currency.
type Platform struct {
	ID           int    `json:"id"`
	Name         string `json:"name"`
	Symbol       string `json:"symbol"`
	Slug         string `json:"slug"`
	TokenAddress string `json:"token_address"`
}

// Price models the data from coinmarketcap for a crypto currency.
type Price struct {
	ID                     int                 `json:"id"`
	Name                   string              `json:"name"`
	Symbol                 string              `json:"symbol"`
	Slug                   string              `json:"slug"`
	IsActive               int                 `json:"is_active"`
	IsFiat                 int                 `json:"is_fiat"`
	CmcRank                int                 `json:"cmc_rank"`
	NumMarketPairs         int                 `json:"num_market_pairs"`
	CirculatingSupply      float64             `json:"circulating_supply"`
	TotalSupply            float64             `json:"total_supply"`
	MarketCapByTotalSupply int                 `json:"market_cap_by_total_supply"`
	MaxSupply              float64             `json:"max_supply"`
	DateAdded              time.Time           `json:"date_added"`
	Tags                   []string            `json:"tags"`
	Platform               Platform            `json:"platform"`
	LastUpdated            time.Time           `json:"last_updated"`
	Quote                  map[string]Currency `json:"quote"`
}

// CoinmarketcapResponse models a response from coinmarketcap.
type CoinmarketcapResponse struct {
	Data   []Price `json:"data"`
	Status Status  `json:"status"`
}

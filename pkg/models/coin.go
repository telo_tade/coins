package models

import (
	"fmt"
)

// Coin models a crypto currency, as needed by the UI/API layer.
type Coin struct {
	Name  string  `json:"name"`
	Rank  int     `json:"rank"`
	Price float64 `json:"price"`
}

func (c Coin) String() string {
	return fmt.Sprintf("Coin[Name %v, Rank %v, Price %v]", c.Name, c.Rank, c.Price)
}

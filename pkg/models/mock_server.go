package models

import (
	"net/http"
	"net/http/httptest"
)

// MockRecorder provides a way to record request information from every successful request.
type MockRecorder interface {
	Record(r *http.Request)
}

// MockResponse represents a response for the mock server to serve.
type MockResponse struct {
	StatusCode int
	Headers    http.Header
	Body       []byte
}

// MockServerProcedure ties a mock response to a url and a method.
type MockServerProcedure struct {
	URI        string
	HTTPMethod string
	Response   MockResponse
}

// NewMockServer return a mock HTTP server to test requests.
func NewMockServer(rec MockRecorder, procedures ...MockServerProcedure) *httptest.Server {
	handler := http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			for _, proc := range procedures {
				if proc.URI == r.URL.RequestURI() && proc.HTTPMethod == r.Method {
					headers := w.Header()
					for hkey, hvalue := range proc.Response.Headers {
						headers[hkey] = hvalue
					}

					code := proc.Response.StatusCode
					if code == 0 {
						code = http.StatusOK
					}

					w.WriteHeader(code)
					_, err := w.Write(proc.Response.Body)
					if err != nil {
						panic(err)
					}

					if rec != nil {
						rec.Record(r)
					}

					return
				}
			}

			w.WriteHeader(http.StatusNotFound)
		},
	)

	return httptest.NewServer(handler)
}

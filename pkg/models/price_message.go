package models

import "time"

// PriceMessage encapsulates a price reading and a timestamp.
type PriceMessage struct {
	Payload   map[string]float64
	Timestamp time.Time
}

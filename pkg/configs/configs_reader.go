/*
Package configsreader offers functionality to read config data from a yaml file. Its main methods is:
	ReadConfigs(filename string) (*Configs, error)
This function will open the yaml file, read its contents and returns a Cofnig struct with the values it has read.
*/
package configsreader

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/url"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

// Configs models the configuration values needed by the "Usage Collector" module.
type Configs struct {
	Currency              string `json:"currency" yaml:"currency"`
	Host                  string `json:"host" yaml:"host"`
	Port                  uint   `json:"port" yaml:"port"`
	CoinmarketKey         string `json:"coinmarketKey" yaml:"coinmarketKey"`
	CoinmarketEndpoint    string `json:"coinmarketEndpoint" yaml:"coinmarketEndpoint"`
	CryptocompareKey      string `json:"cryptocompareKey" yaml:"cryptocompareKey"`
	CryptocompareEndpoint string `json:"cryptocompareEndpoint" yaml:"cryptocompareEndpoint"`
	PricesQueue           string `json:"pricesQueue" yaml:"pricesQueue"`
	RankingsQueue         string `json:"rankingsQueue" yaml:"rankingsQueue"`
}

func (c Configs) String() string {
	return fmt.Sprintf("Configs[Currency %v, DBHost %v, DBPort %v, PricesQueue %v, RankingsQueue %v]",
		c.Currency, c.Host, c.Port, c.PricesQueue, c.RankingsQueue)
}

// ReadConfigs reads configs form a yaml file, returns a Cofnig struct with the values it has read.
func ReadConfigs() (*Configs, error) {
	filename := flag.String("config", "config/config.yaml", "Absolute or relative path to the config file")
	flag.Parse()

	yamlFile, err := ioutil.ReadFile(*filename)
	if err != nil {
		return nil, errors.Wrap(err, "Reading the config file at "+*filename+" failed")
	}

	result := Configs{}

	err = yaml.Unmarshal(yamlFile, &result)
	if err != nil {
		return nil, errors.Wrap(err, "Unmarshalling the content of the config file failed")
	}

	_, urlErr := url.ParseRequestURI(result.CoinmarketEndpoint)
	if urlErr != nil {
		return nil, errors.New(fmt.Sprintf("Invalid config parameter CoinmarketEndpoint: %v", result.CoinmarketEndpoint))
	}

	_, urlErr = url.ParseRequestURI(result.CryptocompareEndpoint)
	if urlErr != nil {
		return nil, errors.New(fmt.Sprintf("Invalid config CryptocompareEndpoint: %v", result.CryptocompareEndpoint))
	}

	_, urlErr = url.ParseRequestURI(result.PricesQueue)
	if urlErr != nil {
		return nil, errors.New(fmt.Sprintf("Invalid config PricesQueue: %v", result.PricesQueue))
	}

	_, urlErr = url.ParseRequestURI(result.RankingsQueue)
	if urlErr != nil {
		return nil, errors.New(fmt.Sprintf("Invalid config RankingsQueue: %v", result.RankingsQueue))
	}

	return &result, nil
}

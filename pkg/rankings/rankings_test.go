package rankings_test

import (
	"context"
	"encoding/json"
	"net/http"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/coins/pkg/models"
	"gitlab.com/telo_tade/coins/pkg/rankings"
)

func TestClient_GetRankings(t *testing.T) {
	data := []models.Data{{CoinInfo: models.CoinInfo{Algorithm: "SHA-256", Name: "BTC", ID: "1182", FullName: "Bitcoin"}}}

	expectedResponse := models.RankResponse{Message: "Success", Data: data, HasWarning: false, Type: 100}
	expectedJSON, err := json.Marshal(expectedResponse)
	assert.Nil(t, err, "Error should be nil.")

	mockServer := models.NewMockServer(nil, models.MockServerProcedure{
		URI:        "/data/top/totalvolfull?api_key=abcd1234&limit=10&tsym=USD",
		HTTPMethod: http.MethodGet,
		Response:   models.MockResponse{StatusCode: http.StatusOK, Body: expectedJSON},
	})

	type fields struct {
		URL string
		Key string
	}

	type args struct {
		ctx   context.Context
		limit int
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		{
			name:    "Normal return",
			args:    args{ctx: context.Background(), limit: 10},
			fields:  fields{Key: "abcd1234", URL: mockServer.URL + "/data/top/totalvolfull"},
			wantErr: false,
			want:    []string{"Bitcoin"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &rankings.Client{URL: tt.fields.URL, Key: tt.fields.Key, Currency: "USD"}

			got, err := c.GetRankings(tt.args.ctx, tt.args.limit)

			if (err != nil) != tt.wantErr {
				t.Errorf("Client.GetRankings() error = %v, wantErr %v", err, tt.wantErr)

				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Client.GetRankings() = %v, want %v", got, tt.want)
			}
		})
	}
}

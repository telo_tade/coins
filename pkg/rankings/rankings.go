package rankings

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/telo_tade/coins/pkg/models"
)

var errCreateRequest = errors.New("Error creating request")

var errSendingRequest = errors.New("Error sending request to server")

var errTooSmall = errors.New("Limit should be greater or equal to 10")

var errTooBig = errors.New("Limit should be <= 100")

var errNoRankings = errors.New("No rankings were found")

// Client encapsulates functionality to get prices from coinmaketcap.
type Client struct {
	URL      string
	Key      string
	Currency string
}

// getCryptocompareRankings returns all rankings.
func (c *Client) getCryptocompareRankings(ctx context.Context, limit int) (*models.RankResponse, error) {
	if limit < 10 {
		return nil, errTooSmall
	}

	if limit > 100 {
		return nil, errTooBig
	}

	client := &http.Client{Timeout: 5 * time.Second}

	req, err := http.NewRequestWithContext(ctx, "GET", c.URL, nil)
	if err != nil {
		return nil, errCreateRequest
	}

	q := url.Values{}
	q.Add("limit", strconv.Itoa(limit))
	q.Add("api_key", c.Key)
	q.Add("tsym", c.Currency)

	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, errSendingRequest
	}
	defer resp.Body.Close()

	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "Could not read response")
	}

	result := models.RankResponse{}

	err = json.Unmarshal(responseData, &result)
	if err != nil {
		return nil, errors.Wrap(err, "Could not unmarshall response")
	}

	return &result, nil
}

// GetRankings returns all rankings.
func (c *Client) GetRankings(ctx context.Context, limit int) ([]string, error) {
	r, err := c.getCryptocompareRankings(ctx, limit)
	if err != nil {
		return nil, err
	}

	if len(r.Data) == 0 {
		return nil, errNoRankings
	}

	result := make([]string, len(r.Data))

	for i := 0; i < len(r.Data); i++ {
		result[i] = r.Data[i].CoinInfo.FullName
	}

	return result, nil
}

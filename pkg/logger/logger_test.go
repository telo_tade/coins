package logger_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/coins/pkg/logger"
)

// TestInitLogger tests init logger functionality.
func TestInitLogger(t *testing.T) {
	l := logger.InitLogger()

	assert.NotNil(t, l, "Nil logger returned.")
}

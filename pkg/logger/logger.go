package logger

import (
	"github.com/go-log/log"
	"github.com/go-log/log/print"
	"github.com/sirupsen/logrus"
)

// InitLogger instantiates a logger object.
func InitLogger() log.Logger {
	result := print.New(logrus.WithFields(logrus.Fields{}))

	return result
}

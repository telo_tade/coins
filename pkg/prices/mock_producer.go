package prices

import (
	"encoding/json"
	"math/rand"
	"time"

	"github.com/go-log/log"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"gitlab.com/telo_tade/coins/pkg/models"
)

// JobMockClient places mocked prices into the price queue.
type JobMockClient struct {
	l          log.Logger
	queueURL   string
	connection *amqp.Connection
	channel    *amqp.Channel
	queue      *amqp.Queue
}

// NewJobMockClient returns a new JobMockClient.
func NewJobMockClient(l log.Logger, queueURL string) (*JobMockClient, error) {
	result := JobMockClient{
		l:        l,
		queueURL: queueURL,
	}

	connection, err := amqp.Dial(queueURL)
	if err != nil {
		l.Logf("Error dialing amqp: %v", err.Error())

		return nil, errors.Wrap(err, "Error dialing amqp")
	}

	amqpChannel, err := connection.Channel()
	if err != nil {
		l.Logf("Error creating channel: %v", err.Error())
		connection.Close()

		return nil, errors.Wrap(err, "Error creating channel")
	}

	queue, err := amqpChannel.QueueDeclare(queueName, true, false, false, false, nil)
	if err != nil {
		l.Logf("Error declaring queue: %v", err.Error())
		connection.Close()
		amqpChannel.Close()

		return nil, errors.Wrap(err, "Error declaring queue")
	}

	result.connection = connection
	result.channel = amqpChannel
	result.queue = &queue

	return &result, nil
}

// Close will close the channel & connection.
func (j *JobMockClient) Close() {
	if j.channel != nil {
		j.channel.Close()
	}

	if j.connection != nil {
		j.connection.Close()
	}
}

// Execute periodically adds mock prices to the prices queue.
func (j *JobMockClient) Execute() {
	start := time.Now()
	defer j.logDuration(start)
	j.l.Logf("Starting to execute mock price producer cmd: %v", start)

	for {
		prices := mockPrices()

		body, err := json.Marshal(models.PriceMessage{Payload: prices, Timestamp: time.Now()})
		if err != nil {
			j.l.Logf("Error marshalling %v: %v", prices, err.Error())

			return
		}

		err = j.channel.Publish("", queueName, false, false, amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         body,
		})

		if err != nil {
			j.l.Logf("Error publishing prices: %v", err.Error())
		} else {
			j.l.Logf("Successfully published to message queue.")
		}

		time.Sleep(50 * time.Millisecond)
	}
}

func (j *JobMockClient) logDuration(start time.Time) {
	j.l.Logf("Started at %v, It took %v", start, time.Since(start))
}

var cryptocurrencies = []string{
	"Bitcoin", "Ethereum", "Chainlink", "Bitcoin Cash", "XRP",
	"yearn.finance", "TRON", "EOS", "Litecoin", "DFI.money", "Cardano", "Nasdaq 100 Index",
	"Binance Coin", "Bitcoin SV", "Polkadot", "Monero", "UMA", "BitTorrent", "Tether", "ZCash",
	"OMG Network", "Uniswap Protocol Token", "JUST", "Ethereum Classic", "NEO", "Tezos", "Stellar",
	"Crypto.com Chain Token", "Okex", "Simple Token", "Cosmos", "Band Protocol", "VeChain", "Ontology",
	"USD Coin", "S&P 500", "QTUM", "Dash", "Aave", "Curve DAO Token", "Compound", "Algorand", "Sushi",
	"Swipe", "Storj", "NEM", "ABBC Coin", "Theta", "Huobi Token", "Dow Jones 30", "Zilliqa", "Waves",
	"TenX", "IOS token", "Mainframe", "0x", "Seele", "Dogecoin", "BKEX Token", "Celo", "Thorecoin",
	"Kcash", "Thunder Token", "Civic", "FileCoin", "Wrapped Bitcoin", "Maker", "Paxos Standard",
	"Orchid Protocol", "Kusama", "Synthetix", "Balancer", "Basic Attention Token", "Kyber Network",
	"Nest Protocol", "Tellor", "Compound Ethereum", "REN", "GXChain", "Serum", "ICON Project",
	"Loopring", "True USD", "Nuls", "Metal", "GIFTO", "Tesla", "IOTA", "AirSwap", "WaykiChain", "Klaytn",
	"Refereum", "Fantom", "Kava", "HyperCash", "ZB", "True Chain", "aelf", "LinkEye", "Reserve Rights",
}

func mockPrices() map[string]float64 {
	result := make(map[string]float64, 100)

	rand.Seed(time.Now().UnixNano())

	for _, c := range cryptocurrencies {
		//nolint:gosec
		result[c] = rand.Float64() * 100
	}

	return result
}

package prices

import (
	"context"
	"encoding/json"
	"time"

	"github.com/go-log/log"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"gitlab.com/telo_tade/coins/pkg/models"
)

const queueName = "prices_queue"

// JobClient encapsulates prices prodcer functionality. It is meant to be called by a job.
type JobClient struct {
	l          log.Logger
	queueURL   string
	client     Client
	connection *amqp.Connection
	channel    *amqp.Channel
	queue      *amqp.Queue
}

// NewJobClient returns a new JobClient.
func NewJobClient(l log.Logger, queueURL string, c Client) (*JobClient, error) {
	result := JobClient{
		l:        l,
		queueURL: queueURL,
		client:   c,
	}

	connection, err := amqp.Dial(queueURL)
	if err != nil {
		l.Logf("Error dialing amqp: %v", err.Error())

		return nil, errors.Wrap(err, "Error dialing amqp")
	}

	amqpChannel, err := connection.Channel()
	if err != nil {
		l.Logf("Error creating channel: %v", err.Error())
		connection.Close()

		return nil, errors.Wrap(err, "Error creating channel")
	}

	queue, err := amqpChannel.QueueDeclare(queueName, true, false, false, false, nil)
	if err != nil {
		l.Logf("Error declaring queue: %v", err.Error())
		connection.Close()
		amqpChannel.Close()

		return nil, errors.Wrap(err, "Error declaring queue")
	}

	result.connection = connection
	result.channel = amqpChannel
	result.queue = &queue

	return &result, nil
}

// Close will close the channel & connection.
func (j *JobClient) Close() {
	if j.channel != nil {
		j.channel.Close()
	}

	if j.connection != nil {
		j.connection.Close()
	}
}

// Execute periodically gets prices and adds to the prices queue.
func (j *JobClient) Execute() {
	start := time.Now()
	defer j.logDuration(start)
	j.l.Logf("Starting to execute producer cmd: %v", start)

	for {
		prices, err := j.client.GetPrices(context.Background())
		if err != nil {
			j.l.Logf("Error getting prices: %v", err.Error())

			return
		}

		body, err := json.Marshal(models.PriceMessage{Payload: prices, Timestamp: time.Now()})
		if err != nil {
			j.l.Logf("Error marshalling %v: %v", prices, err.Error())

			return
		}

		err = j.channel.Publish("", queueName, false, false, amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         body,
		})

		if err != nil {
			j.l.Logf("Error publishing prices: %v", err.Error())
		} else {
			j.l.Logf("Successfully published to message queue.")
		}

		time.Sleep(50 * time.Millisecond)
	}
}

func (j *JobClient) logDuration(start time.Time) {
	j.l.Logf("Started at %v, It took %v", start, time.Since(start))
}

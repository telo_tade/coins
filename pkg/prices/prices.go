package prices

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/telo_tade/coins/pkg/models"
)

var errCreateRequest = errors.New("Error creating request")

var errSendingRequest = errors.New("Error sending request to server")

var errNoPrices = errors.New("No prices were found")

// Client encapsulates functionality to get prices from coinmaketcap.
type Client struct {
	URL      string
	Key      string
	Currency string
}

// getCoinmarketPrices returns all prices.
func (c *Client) getCoinmarketPrices(ctx context.Context) (*models.CoinmarketcapResponse, error) {
	client := &http.Client{Timeout: 5 * time.Second}

	req, err := http.NewRequestWithContext(ctx, "GET", c.URL, nil)
	if err != nil {
		return nil, errCreateRequest
	}

	q := url.Values{}
	q.Add("start", "1")
	q.Add("convert", c.Currency)

	req.Header.Set("Accepts", "application/json")
	req.Header.Add("X-CMC_PRO_API_KEY", c.Key)
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, errSendingRequest
	}
	defer resp.Body.Close()

	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "Could not read response")
	}

	result := models.CoinmarketcapResponse{}

	err = json.Unmarshal(responseData, &result)
	if err != nil {
		return nil, errors.Wrap(err, "Could not unmarshall response")
	}

	return &result, nil
}

// GetPrices returns all prices.
func (c *Client) GetPrices(ctx context.Context) (map[string]float64, error) {
	r, err := c.getCoinmarketPrices(ctx)
	if err != nil {
		return nil, err
	}

	if len(r.Data) == 0 {
		return nil, errNoPrices
	}

	result := make(map[string]float64, len(r.Data))

	for i := 0; i < len(r.Data); i++ {
		result[r.Data[i].Name] = r.Data[i].Quote[c.Currency].Price
	}

	return result, nil
}

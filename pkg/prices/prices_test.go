package prices_test

import (
	"context"
	"encoding/json"
	"net/http"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/coins/pkg/models"
	"gitlab.com/telo_tade/coins/pkg/prices"
)

func TestClient_GetPrices(t *testing.T) {
	quotes := make(map[string]models.Currency)
	quotes["USD"] = models.Currency{MarketCap: 123.33, Price: 3.3, LastUpdated: time.Now().UTC()}
	data := []models.Price{{ID: 213, Name: "BTC", DateAdded: time.Now().UTC(), IsActive: 1, Quote: quotes}}
	expectedResponse := models.CoinmarketcapResponse{
		Data:   data,
		Status: models.Status{Timestamp: time.Now().UTC(), CreditCount: 1, Elapsed: 2, ErrorCode: 0, ErrorMessage: ""},
	}
	expectedJSON, err := json.Marshal(expectedResponse)
	assert.Nil(t, err, "Error should be nil.")

	mockServer := models.NewMockServer(nil, models.MockServerProcedure{
		URI:        "/coins?convert=USD&start=1",
		HTTPMethod: http.MethodGet,
		Response:   models.MockResponse{StatusCode: http.StatusOK, Body: expectedJSON},
	})

	type fields struct {
		URL string
		Key string
	}

	type args struct {
		ctx context.Context
	}

	tests := []struct {
		name     string
		fields   fields
		args     args
		expected map[string]float64
	}{
		{
			name:     "Normal return",
			args:     args{ctx: context.Background()},
			fields:   fields{Key: "a", URL: mockServer.URL + "/coins?limit=20"},
			expected: map[string]float64{"BTC": 3.3},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &prices.Client{URL: tt.fields.URL, Key: tt.fields.Key, Currency: "USD"}

			result, err := c.GetPrices(tt.args.ctx)
			assert.Nil(t, err, "Error should be nil.")
			assert.NotNil(t, result, "Result should not be nil.")
			assert.True(t, reflect.DeepEqual(result, tt.expected), "Result does not equal expected.")
		})
	}
}

package inspector

import (
	"time"

	"github.com/go-log/log"
	"github.com/streadway/amqp"
)

const pricesQueueName = "prices_queue"

const rankingsQueueName = "rankings_queue"

// Client encapsulates inspector functionality.
type Client struct {
	L                log.Logger
	PricesQueueURL   string
	RankingsQueueURL string
}

func (c *Client) createConnections() (*amqp.Connection, *amqp.Connection) {
	pricesConn, err := amqp.Dial(c.PricesQueueURL)
	if err != nil {
		c.L.Logf("Error creating connection for %v: %v", c.PricesQueueURL, err.Error())
	}

	rankingsConn, err := amqp.Dial(c.RankingsQueueURL)
	if err != nil {
		c.L.Logf("Error creating connection for %v: %v", c.RankingsQueueURL, err.Error())
	}

	return pricesConn, rankingsConn
}

func (c *Client) closeConnections(p, r *amqp.Connection) {
	p.Close()
	r.Close()
}

func (c *Client) createChannels(p, r *amqp.Connection) (*amqp.Channel, *amqp.Channel) {
	prices, err := p.Channel()
	if err != nil {
		c.L.Logf("Error creating channel for connection %v", p)
	}

	rankings, err := r.Channel()
	if err != nil {
		c.L.Logf("Error creating channel for connection %v", r)
	}

	return prices, rankings
}

func (c *Client) closeChannels(p, r *amqp.Channel) {
	p.Close()
	r.Close()
}

func (c *Client) declareQueue(p, r *amqp.Channel) (*amqp.Queue, *amqp.Queue) {
	prices, err := p.QueueDeclare(pricesQueueName, true, false, false, false, nil)
	if err != nil {
		c.L.Logf("Error declaring queue: %v", err.Error())
	}

	rankings, err := r.QueueDeclare(rankingsQueueName, true, false, false, false, nil)
	if err != nil {
		c.L.Logf("Error declaring queue: %v", err.Error())
	}

	return &prices, &rankings
}

// Execute inspecting the queues.
func (c *Client) Execute() {
	start := time.Now()
	defer c.logDuration(start)
	c.L.Logf("Starting to execute inspector cmd: %v", start)

	pricesConn, rankingsConn := c.createConnections()
	if pricesConn == nil || rankingsConn == nil {
		return
	}

	defer c.closeConnections(pricesConn, rankingsConn)

	pricesChan, rankingsChan := c.createChannels(pricesConn, rankingsConn)
	if pricesChan == nil || rankingsChan == nil {
		return
	}

	defer c.closeChannels(pricesChan, rankingsChan)

	for {
		pricesQueue, rankingsQueue := c.declareQueue(pricesChan, rankingsChan)

		if pricesQueue == nil {
			c.L.Logf("\npricesQueue is nil!\n")
		} else {
			c.L.Logf("\nQueue %v, URL %v has:\n		%d consumers and %d messages\n",
				pricesQueueName, c.PricesQueueURL, pricesQueue.Consumers, pricesQueue.Messages)
		}

		if rankingsQueue == nil {
			c.L.Logf("\rankingsQueue is nil!\n")
		} else {
			c.L.Logf("\nQueue %v, URL %v has:\n		%d consumers and %d messages\n",
				rankingsQueueName, c.RankingsQueueURL, rankingsQueue.Consumers, rankingsQueue.Messages)
		}

		time.Sleep(1 * time.Second)
	}
}

func (c *Client) logDuration(start time.Time) {
	c.L.Logf("Started at %v, It took %v", start, time.Since(start))
}

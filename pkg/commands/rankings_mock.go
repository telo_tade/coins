package commands

import (
	"context"
	"flag"

	"github.com/google/subcommands"
)

// RankingsMock represents a cmd line command.
type RankingsMock struct {
	Producer rankingsExecutor
}

// Name returns the name of the command.
func (p *RankingsMock) Name() string { return "rankingsMock" }

// Synopsis returns a short string (less than one line) describing the command.
func (p *RankingsMock) Synopsis() string { return "Start this app in rankingsMock mode." }

// Usage returns a long string explaining the command and giving usage information.
func (p *RankingsMock) Usage() string {
	return "Start the app in rankingsMock producer mode.\n" +
		"Every 50 milliseconds adds mock rankings to a message queue.\n"
}

// SetFlags adds the flags for this command to the specified set.
func (p *RankingsMock) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (p *RankingsMock) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	p.Producer.Execute()

	return subcommands.ExitSuccess
}

package commands

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"syscall"

	"github.com/google/subcommands"
)

// Connected represents a cmd line command.
type Connected struct {
	Server starter
}

type starter interface {
	Start()
}

// Name returns the name of the command.
func (*Connected) Name() string { return "connected" }

// Synopsis returns a short string (less than one line) describing the command.
func (*Connected) Synopsis() string { return "Start this app in connected mode." }

// Usage returns a long string explaining the command and giving usage information.
func (*Connected) Usage() string {
	return "Start the app in connected mode. This will start a server which provides a rest API.\n" +
		"Its purpose is to list the top most expensive crypto currencies.\n" +
		"It does so by combining data from the prices and the rankings message queues.\n"
}

// SetFlags adds the flags for this command to the specified set.
func (*Connected) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (c *Connected) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	c.Server.Start()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	<-sig

	return subcommands.ExitSuccess
}

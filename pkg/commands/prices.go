package commands

import (
	"context"
	"flag"

	"github.com/google/subcommands"
)

// Prices represents a cmd line command.
type Prices struct {
	Producer executor
}

type executor interface {
	Execute()
}

// Name returns the name of the command.
func (p *Prices) Name() string { return "prices" }

// Synopsis returns a short string (less than one line) describing the command.
func (p *Prices) Synopsis() string { return "Start this app in prices mode." }

// Usage returns a long string explaining the command and giving usage information.
func (p *Prices) Usage() string {
	return "Start the app in prices producer mode.\n" +
		"It will read prices every 50 milliseconds and add them to a message queue.\n" +
		"The prices are read from coinmarket.\n"
}

// SetFlags adds the flags for this command to the specified set.
func (p *Prices) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (p *Prices) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	p.Producer.Execute()

	return subcommands.ExitSuccess
}

package commands

import (
	"context"
	"flag"

	"github.com/google/subcommands"
)

// PricesMock represents a cmd line command.
type PricesMock struct {
	Producer executor
}

// Name returns the name of the command.
func (p *PricesMock) Name() string { return "pricesMock" }

// Synopsis returns a short string (less than one line) describing the command.
func (p *PricesMock) Synopsis() string { return "Start this app in pricesMock mode." }

// Usage returns a long string explaining the command and giving usage information.
func (p *PricesMock) Usage() string {
	return "Start the app in pricesMock producer mode.\n" +
		"Every 50 milliseconds adds mock prices to a message queue.\n"
}

// SetFlags adds the flags for this command to the specified set.
func (p *PricesMock) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (p *PricesMock) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	p.Producer.Execute()

	return subcommands.ExitSuccess
}

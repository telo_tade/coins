// +build !race

package commands_test

import (
	"context"
	"flag"
	"syscall"
	"testing"
	"time"

	"github.com/google/subcommands"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/coins/pkg/commands"
)

type connectedStarterMock struct {
	count int
}

func (s *connectedStarterMock) Start() {
	s.count++
}

func TestConnected_Execute(t *testing.T) {
	type fields struct {
		Server *connectedStarterMock
	}

	type args struct {
		in0 context.Context
		in1 *flag.FlagSet
		in2 []interface{}
	}

	tests := []struct {
		name           string
		fields         fields
		args           args
		wantExitStatus subcommands.ExitStatus
	}{
		{
			name: "Connected cmd should call Server.Start()",
			args: args{
				in0: context.Background(),
				in1: nil,
				in2: nil,
			},
			wantExitStatus: subcommands.ExitSuccess,
			fields:         fields{Server: &connectedStarterMock{}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cmd := commands.Connected{
				Server: tt.fields.Server,
			}

			result := subcommands.ExitFailure

			go func() {
				result = cmd.Execute(tt.args.in0, tt.args.in1, tt.args.in2...)
			}()

			time.Sleep(500 * time.Millisecond)

			err := syscall.Kill(syscall.Getpid(), syscall.SIGINT)
			assert.Nil(t, err, "Error should be nil.")

			time.Sleep(10 * time.Millisecond) // needs this for result to be updated

			assert.Equal(t, tt.wantExitStatus, result, "Wrong result.")
			assert.Equal(t, 1, tt.fields.Server.count, "Starter should have been called.")
		})
	}
}

package commands

import (
	"context"
	"flag"

	"github.com/google/subcommands"
)

// Rankings represents a cmd line command.
type Rankings struct {
	Producer rankingsExecutor
}

// I want to be able to copy paste this file and still work, that is why I am not simply using executor.
type rankingsExecutor interface {
	Execute()
}

// Name returns the name of the command.
func (r *Rankings) Name() string { return "rankings" }

// Synopsis returns a short string (less than one line) describing the command.
func (r *Rankings) Synopsis() string { return "Start this app in rankings mode." }

// Usage returns a long string explaining the command and giving usage information.
func (r *Rankings) Usage() string {
	return "Start the app in rankings producer mode.\n" +
		"It will read rankings every 50 milliseconds and add them to a message queue.\n" +
		"The rankings are read from cryptocompare.\n"
}

// SetFlags adds the flags for this command to the specified set.
func (r *Rankings) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (r *Rankings) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	r.Producer.Execute()

	return subcommands.ExitSuccess
}

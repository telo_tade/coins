package commands

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"syscall"

	"github.com/google/subcommands"
)

// Standalone represents a cmd line command.
type Standalone struct {
	Server standaloneStarter
}

type standaloneStarter interface {
	Start()
}

// Name returns the name of the command.
func (*Standalone) Name() string { return "standalone" }

// Synopsis returns a short string (less than one line) describing the command.
func (*Standalone) Synopsis() string { return "Start this app in standalone mode." }

// Usage returns a long string explaining the command and giving usage information.
func (*Standalone) Usage() string {
	return "Start the app in standalone mode. This will start a server which provides a rest API.\n" +
		"Its purpose is to list the top most expensive crypto currencies."
}

// SetFlags adds the flags for this command to the specified set.
func (*Standalone) SetFlags(_ *flag.FlagSet) {}

// Execute executes the command and returns an ExitStatus.
func (s *Standalone) Execute(_ context.Context, _ *flag.FlagSet, _ ...interface{}) (exitStatus subcommands.ExitStatus) {
	s.Server.Start()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	<-sig

	return subcommands.ExitSuccess
}

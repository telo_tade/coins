package commands_test

import (
	"context"
	"flag"
	"testing"

	"github.com/google/subcommands"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/coins/pkg/commands"
)

type rankingsExecutorMock struct {
	count int
}

func (e *rankingsExecutorMock) Execute() {
	e.count++
}

func TestRankings_Execute(t *testing.T) {
	type fields struct {
		Producer rankingsExecutorMock
	}

	type args struct {
		in0 context.Context
		in1 *flag.FlagSet
		in2 []interface{}
	}

	tests := []struct {
		name           string
		fields         fields
		args           args
		wantExitStatus subcommands.ExitStatus
	}{
		{
			name: "normal execution",
			args: args{
				in0: context.Background(),
				in1: nil,
				in2: nil,
			},
			fields: fields{
				Producer: rankingsExecutorMock{},
			},
			wantExitStatus: subcommands.ExitSuccess,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := commands.Rankings{
				Producer: &tt.fields.Producer,
			}

			gotExitStatus := p.Execute(tt.args.in0, tt.args.in1, tt.args.in2...)
			assert.True(t, subcommands.ExitSuccess == gotExitStatus, "Should exit with success.")
			assert.Equal(t, 1, tt.fields.Producer.count, "Execute should have been called once.")
		})
	}
}

package service

import (
	"context"

	"github.com/go-log/log"
	"github.com/pkg/errors"
	"gitlab.com/telo_tade/coins/pkg/combine"
	"gitlab.com/telo_tade/coins/pkg/models"
)

// StandaloneService holds the business logic for running in standalone mode.
type StandaloneService struct {
	Logger      log.Logger
	PriceGetter priceGetter
	RankGetter  rankGetter
}

type priceGetter interface {
	GetPrices(ctx context.Context) (map[string]float64, error)
}

type rankGetter interface {
	GetRankings(ctx context.Context, limit int) ([]string, error)
}

// GetAssets returns the most expensive crypto currencies.
func (s *StandaloneService) GetAssets(ctx context.Context, limit int) ([]models.Coin, error) {
	ranks, err := s.RankGetter.GetRankings(ctx, limit)
	if err != nil {
		return nil, errors.Wrap(err, "Could not get rankings")
	}

	prices, err := s.PriceGetter.GetPrices(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "Could not get prices")
	}

	return combine.Combine(ranks, prices)
}

// ConnectedService holds the business logic for running in connected mode.
// It reads from 2 message queues to compose the result.
type ConnectedService struct {
	Logger      log.Logger
	AssetGetter assetGetter
}

type assetGetter interface {
	GetAssets() ([]models.Coin, error)
}

// GetAssets returns all 100 assets, with data valid for the last 1 second.
func (c *ConnectedService) GetAssets(ctx context.Context, limit int) ([]models.Coin, error) {
	coins, err := c.AssetGetter.GetAssets()
	if err != nil {
		return []models.Coin{}, err
	}

	if len(coins) < limit {
		return coins, nil
	}

	return coins[:limit], nil
}

package service_test

import (
	"context"
	"reflect"
	"testing"

	"github.com/go-log/log"
	"gitlab.com/telo_tade/coins/pkg/models"
	"gitlab.com/telo_tade/coins/pkg/service"
)

func TestService_GetAssets(t *testing.T) {
	type fields struct {
		Rank   rankGetterMock
		Price  priceGetterMock
		Logger log.Logger
	}

	type args struct {
		ctx   context.Context
		limit int
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []models.Coin
		wantErr bool
	}{
		{
			name: "normal", args: args{ctx: context.Background(), limit: 20}, want: nil, wantErr: true,
			fields: fields{Logger: t, Price: priceGetterMock{}, Rank: rankGetterMock{}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.StandaloneService{
				Logger:      tt.fields.Logger,
				PriceGetter: &tt.fields.Price,
				RankGetter:  &tt.fields.Rank,
			}

			got, err := s.GetAssets(tt.args.ctx, tt.args.limit)
			if (err != nil) != tt.wantErr {
				t.Errorf("Service.GetAssets() error = %v, wantErr %v", err, tt.wantErr)

				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Service.GetAssets() = %v, want %v", got, tt.want)
			}
		})
	}
}

type priceGetterMock struct{}

func (p *priceGetterMock) GetPrices(ctx context.Context) (map[string]float64, error) {
	return map[string]float64{}, nil
}

type rankGetterMock struct{}

func (r *rankGetterMock) GetRankings(ctx context.Context, limit int) ([]string, error) {
	return []string{}, nil
}

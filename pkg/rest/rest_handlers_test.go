package rest_test

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-log/log"
	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/coins/pkg/models"
	"gitlab.com/telo_tade/coins/pkg/rest"
)

func TestHandler_GetAssets(t *testing.T) {
	req, err := http.NewRequestWithContext(context.Background(), "GET", "localhost:8080/coins?limit=20", nil)
	assert.Nil(t, err, "Error creating request: %v.", err)

	type fields struct {
		Logger log.Logger
	}

	type args struct {
		w http.ResponseWriter
		r *http.Request
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "GetAssets returns mocked object",
			args: args{
				w: httptest.NewRecorder(),
				r: req,
			},
			fields: fields{
				Logger: t,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := rest.Handler{
				Logger:  tt.fields.Logger,
				Service: &assetsGetterMock{},
			}

			h.GetAssets(tt.args.w, tt.args.r)

			result := tt.args.w.(*httptest.ResponseRecorder).Result()
			assert.NotNil(t, result, "Response should not be nil.")
			assert.Equal(t, http.StatusOK, result.StatusCode, "Status code should be http.StatusOK.")

			defer result.Body.Close()

			body, err := ioutil.ReadAll(result.Body)
			assert.Nil(t, err, "readAll should produce no error: %v", err)

			var actual []models.Coin
			err = json.Unmarshal(body, &actual)
			assert.Nil(t, err, "Error should be nil.")
			assert.True(t, len(actual) == 0, "Wrong result value.")
		})
	}
}

type assetsGetterMock struct{}

func (a *assetsGetterMock) GetAssets(ctx context.Context, limit int) ([]models.Coin, error) {
	return []models.Coin{}, nil
}

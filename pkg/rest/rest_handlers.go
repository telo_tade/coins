package rest

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/go-log/log"
	"gitlab.com/telo_tade/coins/pkg/models"
)

// Handler stores common objects needed by all handlers.
type Handler struct {
	Logger  log.Logger
	Service assetsGetter
}

type assetsGetter interface {
	GetAssets(ctx context.Context, limit int) ([]models.Coin, error)
}

// GetAssets returns the most expensive crypto currencies.
func (h *Handler) GetAssets(w http.ResponseWriter, r *http.Request) {
	now := time.Now()
	defer h.logDuration(now)
	h.Logger.Logf("GetAssets: %v", now)

	var err error

	limit := 0
	limitString := r.URL.Query().Get("limit")

	if limitString != "" {
		limit, err = strconv.Atoi(limitString)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			h.Logger.Logf("Request does not contain a valid limit parameter.")

			return
		}
	}

	assets, err := h.Service.GetAssets(r.Context(), limit)
	if err != nil {
		h.Logger.Logf("Could not retrieve assets: %v.", err)
		w.WriteHeader(http.StatusNotFound)

		return
	}

	h.writeJSONResponse(assets, http.StatusOK, w)
}

// HealthCheck returns "pong".
func (h *Handler) HealthCheck(w http.ResponseWriter, r *http.Request) {
	if _, err := w.Write([]byte("pong")); err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		h.Logger.Logf("error writing response body, %v", err)
	}
}

func (h *Handler) writeJSONResponse(resp interface{}, code int, w http.ResponseWriter) {
	w.WriteHeader(code)
	w.Header().Set("Content-Type", "application/json")

	body, err := json.Marshal(resp)
	if err != nil {
		h.Logger.Logf("error when marshalling result to json, %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	_, err = w.Write(body)
	if err != nil {
		h.Logger.Logf("error writing response body, %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}
}

func (h *Handler) logDuration(start time.Time) {
	h.Logger.Logf("Started at %v, It took %v", start, time.Since(start))
}

package main

import (
	"context"
	"flag"
	"os"

	"github.com/google/subcommands"
	"gitlab.com/telo_tade/coins/pkg/combine"
	"gitlab.com/telo_tade/coins/pkg/commands"
	configsreader "gitlab.com/telo_tade/coins/pkg/configs"
	"gitlab.com/telo_tade/coins/pkg/inspector"
	"gitlab.com/telo_tade/coins/pkg/logger"
	"gitlab.com/telo_tade/coins/pkg/prices"
	"gitlab.com/telo_tade/coins/pkg/rankings"
	"gitlab.com/telo_tade/coins/pkg/rest"
	"gitlab.com/telo_tade/coins/pkg/service"
)

// GitCommit is used to inject the commit number.
var GitCommit = "local_build"

// Version is used to inject the version number.
var Version = "unknown"

func main() {
	configs, err := configsreader.ReadConfigs()
	if err != nil {
		panic(err.Error())
	}

	l := logger.InitLogger()
	l.Logf("µS - commit: %v, version: %v", GitCommit, Version)

	ranksGetter := rankings.Client{
		Currency: configs.Currency,
		Key:      configs.CryptocompareKey,
		URL:      configs.CryptocompareEndpoint,
	}
	priceGetter := prices.Client{Currency: configs.Currency, Key: configs.CoinmarketKey, URL: configs.CoinmarketEndpoint}
	standaloneService := service.StandaloneService{
		Logger: l, PriceGetter: &priceGetter,
		RankGetter: &ranksGetter,
	}
	standaloneHandler := rest.Handler{Logger: l, Service: &standaloneService}
	standaloneServer := rest.NewServer(configs.Host, configs.Port, l, standaloneHandler)

	assetGetter, err := combine.New(configs.PricesQueue, configs.RankingsQueue, l)
	if err != nil {
		l.Log("could not create assetGetter")
	}

	connectedService := service.ConnectedService{Logger: l, AssetGetter: assetGetter}
	connectedHandler := rest.Handler{Logger: l, Service: &connectedService}
	connectedServer := rest.NewServer(configs.Host, configs.Port, l, connectedHandler)

	pricesJob, err := prices.NewJobClient(l, configs.PricesQueue, priceGetter)
	if err != nil {
		l.Log("could not create pricesJob")
	}

	pricesMockJob, err := prices.NewJobMockClient(l, configs.PricesQueue)
	if err != nil {
		l.Log("could not create pricesMockJob")
	}

	rankingsJob, err := rankings.NewJobClient(l, configs.RankingsQueue, ranksGetter)
	if err != nil {
		l.Log("could not create rankingsJob")
	}

	rankingsMockJob, err := rankings.NewJobMockClient(l, configs.PricesQueue)
	if err != nil {
		l.Log("could not create rankingsMockJob")
	}

	inspect := inspector.Client{L: l, PricesQueueURL: configs.PricesQueue, RankingsQueueURL: configs.RankingsQueue}

	registerSubcommands(standaloneServer, connectedServer, pricesJob, pricesMockJob,
		rankingsJob, rankingsMockJob, &inspect)

	flag.Parse()

	ctx := context.Background()
	os.Exit(int(subcommands.Execute(ctx)))
}

func registerSubcommands(standaloneServer *rest.Server, connectedServer *rest.Server,
	pricesJob *prices.JobClient, pricesMockJob *prices.JobMockClient,
	rankingsJob *rankings.JobClient, rankingsMockJob *rankings.JobMockClient,
	inspect *inspector.Client) {
	subcommands.Register(subcommands.HelpCommand(), "")
	subcommands.Register(subcommands.FlagsCommand(), "")
	subcommands.Register(subcommands.CommandsCommand(), "")

	subcommands.Register(&commands.Standalone{
		Server: standaloneServer,
	}, "")

	subcommands.Register(&commands.Connected{
		Server: connectedServer,
	}, "")

	subcommands.Register(&commands.Prices{
		Producer: pricesJob,
	}, "")

	subcommands.Register(&commands.PricesMock{
		Producer: pricesMockJob,
	}, "")

	subcommands.Register(&commands.Rankings{
		Producer: rankingsJob,
	}, "")

	subcommands.Register(&commands.RankingsMock{
		Producer: rankingsMockJob,
	}, "")

	subcommands.Register(&commands.Inspect{
		Inspector: inspect,
	}, "")
}

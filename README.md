# Price list service for top crypto assets

The service should expose a HTTP endpoint, which when fetched, displays an up-to-date list of top assets and their current prices in USD.

The endpoint should support limit parameter which indicates how many top coins should be returned.
The output should be either JSON or CSV compatible.

## Important: I only return <= top currency

I could not find an endpoint, in cryptocompare.com API's, which returns up to 200 top currencies. The limit parameter is max 100 for all endpoints that accept it. What I did do is get data from this endpoint, which returns up to 100 elements:

min-api.cryptocompare.com/documentation?key=Toplists&cat=TopTotalVolumeEndpointFull

## How to run

Standalone mode:
go run cmd/main/main.go -config=config/config.yaml standalone

Prices mode (adds prices info to a message queue, every ~50 milliseconds):
go run cmd/main/main.go -config=config/config.yaml prices

Rankings mode (adds rankings into a message queue, every ~50 milliseconds):
go run cmd/main/main.go -config=config/config.yaml rankings

Inspector mode (prints info about the queues, every ~1 second):
go run cmd/main/main.go -config=config/config.yaml inspect

Connected mode (offers an API, but needs the prices & rankings message queues):
go run cmd/main/main.go -config=config/config.yaml connected

PricesMock mode (adds random prices info to a message queue, every ~50 milliseconds):
go run cmd/main/main.go -config=config/config.yaml pricesMock

RankingsMock mode (adds random rankings into a message queue, every ~50 milliseconds):
go run cmd/main/main.go -config=config/config.yaml rankingsMock

## Solution idea - standalone mode

In standalone mode the app offers one REST endpoint, to which you can make a GET call like this:
localhost:8080/coins?limit=20

This will internally call cryptocompare to get the top rankings (I did not know which endpoint they offer for rankings and thus chose a plausibly sounding one), then it calls coinmarket to get the prices (all prices for all possible currencies), then combines this information to produce the final result.

## Solution idea - connected (message queue) mode

I ask for prices multiple times per second (i.e. every 50 milliseconds) and put the {answer, timestamp} in a message queue. Similarly, I ask for rankings every 50 milliseconds and place {rankings, timestamp} in another queue. When I want to compute the final result I read everything (reasonable close to this moment of time) from both queues and disregard all info that is more than 1 second old. At this moment I have a list of price data points, and another list of rankings data points both of them are fresher than 1 second. All I need to do now is find the pair of elements, one from prices list the other from rankings, whose timestamps differences are the smallest. This pair {ranking, prices} will be the one used to compose my final result.

This idea amounts to running 3 µServices:

* one that repeatedly retrieves the rankings and places them in a message queue (rankings / rankingsMock mode)
* one that repeatedly retrieves the prices  and places them in a message queue (prices / pricesMock mode)
* one that exposes a HTTP endpoint to which you can make a GET call for the top X ones (connected mode)

It is important that prices (or pricesMock) and rankings (or rankingsMock) are running and activeley pushing to the queues in the 1 second around the moment of time (before and after) where you make the GET call to localhost:8080/coins?limit=180.

## Algorithm to compile response out of two message queues

In each message queue elements have the following format: (Payload, Timestamp). When I receive a call for top X currencies, I first note down the current moment of time (now). I then start looking at the rankings queue:

* each message that is older than (now - 1 second) will be discarded
* each message that is newer than (now - 1 second) will be placed back into the message queue, and stored down as a possible candidate
* if a message newer than (now + 1 second) I stop looking at the rankings queue

After identically looking at the prices queue, we end up with:

* a list of possible rankings/prices candidates, which are all in the time interval (now - 1 second, now + 1 second)
* the queues still contain all messages that are newer than (now - 1 second), thus other instances/sessions of our app could potentially reuse them (the whole message queue approach is needed in the context of a large number of concurrent requests)
* the algorithm would have iterated through all messages produced in the interval (now - 1 second, now), with good probability; since one instance of our app, processing the queues at moment X, by putting back elements distrubs the order of the messages inside the queue; The more instances operate on the same queue at moment X, the higher chances are for messages to be in the wrong chronological order - I have been trying to prove that a message produces at (X - 1 second + infinetely_small_lamba) would be processed by every instance operating for X. So far I did not succeed in proving this, but it seems reasonably likely.

Once we have two sets of candidates (a list of prices, and one of rankings) it is trivial to find the pair (a, b) where a belongs to prices and b belongs to rankings, so as the time difference between a and b is the smallest of any such possible pairs. I chose O(N^2) to do this because the sizer are smallish, with a heap I would have managed to do this step in O(NlogN).

## Notes

Note1: I added it as an exercise, not a very practical solution. Would be much better if both coinmarketcap and cryptocompare were to offer subscribe functionality. Which they probably offer but I have not checked :)

Note2: CoinMarketCap is sending me emails that I have used my daily limit. Apparently I cannot make requests every 50 milliseconds for ever, without buying a subscription. To combat this, run the pricesMock and rankingsMock, they will fill the queues with random prices/rankings without making calls to coinmarketcap/cryptocompare.

Note3: Make sure the queue is up and running, and contains both prices and rankings, before starting the connected mode µService. I did not fully test error recovery behaviour on all flows.

Note4: I believe the message queue  approach is superior in the following situation: we do not want to overload the 2 endpoints (prices&rankings) with too many requests. If we receive thousands of requests per second  ourselves, it makes no sense to use the standalone approach. This is because we do not want to issue thousands of requests per second to the 2 endpoints (prices&rankings). Assume multiple instances of our software run, every instance only acknowledges the old messages and puts back recent enough ones into its queue. This makes the valid queue data available for other instances of our app, without additional calls to prices&rankings.

Note5: testing was done pretty poorly, and no monitoring. It is only a proof of concept.
